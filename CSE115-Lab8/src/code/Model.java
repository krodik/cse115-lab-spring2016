package code;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

public class Model {

	private HashSet<String> _words;
	private ArrayList<String> _longWords;
	
	public Model(HashSet<String> words, ArrayList<String> longWords) {
		_words = words;
		_longWords = longWords;
	}
	
	public Model(String filename){
		_words = new HashSet<String>();
		_longWords = new ArrayList<String>();
		readFile(filename);
	}

	public void readFile(String filename){
		CSE115FileReader file = new CSE115FileReader(filename);
		while ( file.hasNext() ){
			String word = file.next();
			if ( word.length() == 7 ){
				_longWords.add(word);
			} else if( word.length() < 7) {
				_words.add(word);
			}
		}
	}

	public ArrayList<Character> string2charList(String word){
		ArrayList<Character> analyzed = new ArrayList<Character>();
		
		for(int i=0; i < word.length(); i++){
			analyzed.add(word.charAt(i));	
		}
		return analyzed;
	}
	
	//	Step 3: Is String made up of letters in ArrayList<Character>?
	//			Define a method that takes a String and an ArrayList<Character> as arguments, and returns true if
	//			the all of the characters of the String appear in the ArrayList, and false otherwise.
	//			For example, if the ArrayList<Character> contains the letters from "smooths" (i.e. 's', 'm', 'o', 'o', 't',
	//			'h', and another 's') then "toss" should yield true (since there is one 't', one 'o', and two 's' characters
	//			in the ArrayList. However, "moms" should yield false because there are not two 'm' characters in
	//			the ArrayList.
	//			This is independent of steps 1 and 2. You must name this method anagramOfLetterSubset
	//	
	public boolean anagramOfLetterSubset (String s, ArrayList<Character> c){
		
		for ( int i=0; i < s.length(); i++){
			if ( c.contains(s.charAt(i))){
				c.remove(new Character(s.charAt(i)) );
			} else {
				return false;
			}
		}
		return true;
	}
	
	//	Step 4: Create a HashSet<String> of all the words that can be formed from the letters in a
	//	String.
	//	Define a method that takes a String as an argument a returns a HashSet<String> of all the words
	//	from the word list that are of length 3, 4, 5, or 6 and can be made from the letters in the
	//	argument.
	//	This method relies on the word list having been set up in the constructor (step 1), and uses the
	//  methods defined in steps 2 and 3. You must name this method possibleWords.
//	
	public HashSet<String> possibleWords(String word){
		HashSet<String> words = new HashSet<String>();

		Iterator<String> it = _words.iterator();
		
		while( it.hasNext() ){
			String look = it.next();
			if( anagramOfLetterSubset( look, string2charList(word) ) ){
				words.add(look);
			}
		}
		return words;
	}
	
}
